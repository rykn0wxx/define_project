/**
 * VueMaterial
 * @library
 */
// Lib imports
import Vue from 'vue'
import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.min.css'
import './vue-mat-styles.scss'

Vue.use(VueMaterial)

Vue.material = {
  ...Vue.material,
  theming: {
    enabled: true,
    metaColors: true,
    theme: 'default'
  }
}
