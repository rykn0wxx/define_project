/**
 * Vue
 * @library
 */
// Lib imports
import Vue from 'vue'
import { sync } from 'vuex-router-sync'

// Plugins
import media from '@/plugins/media'

// Global Components
import '@/components/global'

// Custom Styles
import '@/sass/app.scss'

// Application imports
import App from './App'
import store from '@/store'
import router from '@/router'
import { AppConstants } from '@/services/utils'

sync(store, router)
Vue.use(media)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  mq: {
    ...AppConstants.MEDIA
  },
  render: h => h(App)
}).$mount('#app')
