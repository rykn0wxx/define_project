/**
 * Define all of your application routes here
 */

export default [
  {
    path: '/',
    name: 'LayoutsApplication',
    view: 'layouts/Application',
    redirect: '/home',
    children: [
      {
        path: '/home',
        name: 'Home',
        view: 'pages/Home'
      }, {
        path: '/about',
        name: 'About',
        view: 'pages/About'
      }, {
        path: '/demo',
        name: 'Demo',
        view: 'pages/Demo'
      }
    ]
  }
]
