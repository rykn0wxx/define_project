# rykn0wxx

> Define me

## Development
- [X] Setup a basic Vue.js application
- [ ] Integrate a framework, choose only one from below
  - [ ] [Tailwind CSS](https://github.com/tailwindcss/tailwindcss)
  - [ ] [Vue Material](https://github.com/vuematerial/vue-material)
  - [ ] [Material Components Vue](https://github.com/matsp/material-components-vue)
    - configurations were too difficult and many configurations were neeeded for this to work
- [ ] Layout design
  - [ ] Themes
